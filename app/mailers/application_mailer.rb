class ApplicationMailer < ActionMailer::Base
  default from: "<no-reply@social-blood.com>"
  layout 'mailer'
end
