class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  attr_accessor :validate_fields, :distance, :city_name

  # validates_presence_of :name,:phone_no, :blood_type_id, :blood_rh_type_id, if: :validate_fields?
  # validates_presence_of :password_confirmation, :on => :create

  belongs_to :blood_types
  belongs_to :blood_rh_types
  has_many :blood_requests

  reverse_geocoded_by :latitude, :longitude

  mount_uploader :profile_photo,ImageUploader

  def generate_authentication_token
    self.auth_token = SecureRandom.urlsafe_base64(25).tr('lIO0', 'blood-donor')
  end

  def validate_fields?
    validate_fields == 'true' || validate_fields == true
  end

  def self.matched_contacts(external_contacts)
    external_contacts = external_contacts.split ","
    puts external_contacts.length
    valid_contacts = []
    external_contacts = external_contacts.map! do |contact|
      unless contact.length < 8
        confidence_number = (contact.length * 0.7).round
        valid_contacts << contact.delete("^0-9").last(confidence_number)
      end
    end

    phone_regex = Regexp.union(valid_contacts).source
    contact_match_user = []
    internal_contacts = self.select("id, phone_no")
    internal_contacts.each do |internal|
      # if /(#{phone_regex})/.match(internal.phone_no)
      if /(#{phone_regex})/.match(internal.phone_no)
        contact_match_user << internal.id
      end
    end
    return User.where.not(phone_no: nil).find(contact_match_user)
  end
end
