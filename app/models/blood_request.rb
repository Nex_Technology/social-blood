class BloodRequest < ActiveRecord::Base
  belongs_to :user
  belongs_to :request_sender, class_name: "User", primary_key: "id"

  scope :not_accepted, -> { where(:accept_flg => false) }
end
