class Api::RegistrationsController < Devise::RegistrationsController
  respond_to :json
  require 'date'
  #sign_up
  def sign_up

    Rails.logger.debug("My object: #{params}")
    puts "My object: #{params}"

    return render json: { message: "Require email"}, status: 400 if params[:email].blank?

    if params[:email].downcase == "test@test.com"
      return render json: {message: "#{params[:test]} is not a valid email address"}
    end

    return render json: { message: "Require password"}, status: 400 if params[:password].blank?
    return render json: { message: "Require confirm password"}, status: 400 if params[:password_confirmation].blank?
    return render json: { message: "Password do not match"}, status: 400 unless params[:password]== params[:password_confirmation]
    return render json: { message: "Require name"}, status: 400 if params[:name].blank?

    if params[:name].downcase == "test"
      return render json: {message: "Please input a valid name."}
    end

    return render json: { message: "Require phone number"}, status: 400 if params[:phone_no].blank?
    return render json: { message: "Require blood Type"}, status: 400 if params[:blood_type_id].blank?
    return render json: { message: "Require blood RH Type"}, status: 400 if params[:blood_rh_type_id].blank?
    return render json: { message: "Require Location"}, status: 400 if params[:address].blank?

    if User.where(email: params[:email]).present?
      return render json: { message: "Email has already been taken "},status: 400
    end

    user = User.new(email: params[:email],
        password: params[:password],
        name: params[:name],
        birth_date: params[:birth_date],
        phone_no: params[:phone_no],
        blood_type_id: params[:blood_type_id],
        blood_rh_type_id: params[:blood_rh_type_id],
        address: params[:address],
        latitude: params[:latitude],
        longitude: params[:longitude],
        about: params[:about],
        has_pet: params[:has_pet],
        pet: params[:pet])

    user.generate_authentication_token
    # user.validate_fields = false
    if user.save!
      sign_in(:user, user)
      @user = current_user
      respond_to do |format|
        format.html
        format.json {render json:@user , status: 200}
      end
    else
     return render json: { message: user.errors, status: 422}
    end
  end

  # Register with Account Kit
  def ak_signup
    return render json: { message: "Require name"}, status: 400 if params[:name].blank?

    if params[:name].downcase == "test"
      return render json: {message: "Please input a valid name."}
    end

    return render json: { message: "Require phone number"}, status: 400 if params[:phone_no].blank?
    return render json: { message: "Require blood Type"}, status: 400 if params[:blood_type_id].blank?
    return render json: { message: "Require blood RH Type"}, status: 400 if params[:blood_rh_type_id].blank?
    return render json: { message: "Require Location"}, status: 400 if params[:address].blank?
    email = "#{params[:phone_no]}@facebook.com"
    if params[:email].present?
      email = params[:email]
    end

    if User.where(email: email).present?
      return render json: { message: "Email has already been taken "},status: 400
    end

    user = User.new(
      email: email,
      name: params[:name],
      password: SecureRandom.hex[0, 10],
      fb_id: params[:fb_id],
      phone_no: params[:phone_no],
      blood_type_id: params[:blood_type_id],
      blood_rh_type_id: params[:blood_rh_type_id],
      address: params[:address],
      latitude: params[:latitude],
      longitude: params[:longitude],
      about: params[:about],
      has_pet: params[:has_pet],
      pet: params[:pet]
    )
    user.generate_authentication_token
    if user.save!
      sign_in(:user, user)
      @user = current_user
      respond_to do |format|
        format.html
        format.json {render json: @user, status: 200}
      end
    else
      return render json: { message: user.errors }, status: 400
    end
  end

  # Login with Account Kit
  def ak_login
    if params[:phone_no].present?
      user = User.where(phone_no: params[:phone_no]).first
    else
      user = User.where(email: params[:email]).first
    end
    if user.blank?
      return render json: { message: "User does not exist" }, status: 400
    else
      user.generate_authentication_token
      if user.save!
        sign_in(:user, user)
        @user = current_user
        respond_to do |format|
          format.html
          format.json {render json: @user, status: 200}
        end
      else
        return render json: { message: user.errors }, status: 422
      end
    end
  end

  #log_in with fb
  def fb_login
    fb_data = JSON.parse(Net::HTTP.get(URI.parse("https://graph.facebook.com/v2.5/me?fields=id,name,gender,email,birthday,picture.type(large)&access_token=#{params[:fb_access_token]}")))
    return render json: fb_data["error"] if fb_data["error"].present?
    return render json: fb_data["message"] if fb_data["message"].present?
    user = User.where(email: fb_data["email"]).first

    if user.blank?
      password = SecureRandom.hex[0, 10]
      user = User.new(
        email: fb_data["email"],
        name: fb_data["name"],
        password: password
      )
    end

    user.gender = fb_data["gender"] if fb_data["gender"].present?
    user.birth_date = Date.strptime(fb_data["birthday"], '%m/%d/%Y')  if fb_data["birthday"].present?
    user.remote_profile_photo_url = fb_data["picture"]["data"]["url"] if fb_data["picture"].present?
    user.fb_id = fb_data["id"]
    user.generate_authentication_token

    # user.validate_fields = false
    if user.save!
      sign_in(:user, user)
      @user = current_user
      respond_to do |format|
        format.html
        format.json {render json: @user, status: 200}
      end
    else
      return render json: { message: user.errors }, status: 400
    end
  end

  def fb_update
    current_user = User.where(:auth_token => params[:auth_token]).first
    current_user.fb_id = params[:fb_id];
    current_user.remote_profile_photo_url = params[:photo_url]

    if current_user.save!

      render json: current_user, status: 200
    else
      return render json: { message: current_user.errors }, status: 400
    end
  end
end
