class Api::BloodNeedsController < Api::BaseController
  skip_before_filter :verify_authenticity_token
  before_filter :check_auth_token

  def index
    if current_user.latitude.blank? || current_user.latitude.nil? || current_user.longitude.blank? || current_user.longitude.nil?
      return render json: {message: "You need to update your location!"}, status: 400
    else
      all_blood_need = BloodNeed.all
      @blood_need = all_blood_need.near([current_user.latitude, current_user.longitude],50, units: :km )
      @blood_need.each do |r|
      r.distance = Geocoder::Calculations.distance_between([current_user.latitude, current_user.longitude],[r.latitude, r.longitude])
      query = "#{r.latitude},#{r.longitude}"
      first_result = Geocoder.search(query).first
        if first_result.present?
          r.city_name = first_result.city
        end
      end
      @blood_need = @blood_need.sort_by{|data| data.distance}
    end
  end

  def show
    @blood_need = BloodNeed.where(id: params[:id]).first
    return render json: { message: 'Invalid Blood Need'}, status: 400 if @blood_need.blank?

    respond_to do |format|
      format.html
      format.json { render json: @blood_need, status: 200}
    end
  end

  def create
    @blood_need = BloodNeed.new(permit_params)
    if @blood_need.save!
      @blood_need
    else
      return render json: { message: @blood_need.errors}, status: 400
    end
  end

  private
  def permit_params
    params.permit(:phone_no, :blood_type_id, :blood_rh_type_id, :longitude, :latitude)
  end

end
