class Api::SearchController < Api::BaseController

  def index

    Rails.logger.debug("current_user: #{current_user}")
    puts "Search params: #{search_params[:latitude]}, #{search_params[:longitude]} "

    data_collection = []
    return render json: { message: 'Required blood type'} if search_params[:blood_type_id].blank?
    if current_user.present?
      data_collection = User.where("blood_type_id = ? AND id <> ?",search_params[:blood_type_id],current_user.id )
    else
      data_collection = User.where("blood_type_id = ?",search_params[:blood_type_id])
    end

    if search_params[:blood_rh_type_id] == '1'
      data_collection = data_collection.where("blood_rh_type_id = 1 OR blood_rh_type_id = 3")
    elsif search_params[:blood_rh_type_id] == '2'
      data_collection = data_collection.where("blood_rh_type_id = 2 OR blood_rh_type_id   = 3")
    end

    data_collection = data_collection.near([search_params[:latitude].to_f, search_params[:longitude].to_f],50, units: :km )
    data_collection.each do |user|
      if user.latitude.blank? || user.latitude.nil? || user.longitude.blank? || user.longitude.nil?
        user.latitude = 0.0
        user.longitude = 0.0
      end
      user.distance = Geocoder::Calculations.distance_between([search_params[:latitude],search_params[:longitude]],[user.latitude, user.longitude])
      query = "#{user.latitude},#{user.longitude}"
      first_result = Geocoder.search(query).first
      if first_result.present?
        user.city_name = first_result.city
      end
    end
    if data_collection.first.present?
      @results = data_collection.sort_by{|data| data.distance}
    else
      return render nothing: true, status: 204
    end
  end

  private
  def search_params
    params.permit( :longitude, :latitude, :blood_type_id, :blood_rh_type_id)
  end

end
