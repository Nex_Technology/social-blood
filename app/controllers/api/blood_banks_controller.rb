class Api::BloodBanksController < Api::BaseController
  skip_before_filter :verify_authenticity_token
  before_filter :check_auth_token

  def index

    user = User.where( id: current_user.id).first
    return render json: { message: 'Invalid User!'} if user.blank?

    @blood_banks = BloodBank.near([params[:latitude], params[:longitude]],50, units: :km )
    result = []
    @blood_banks.each do |blood_bank|
      # get the distance between user and the blood banks
      blood_bank.distance = Geocoder::Calculations.distance_between([user.latitude,user.longitude],[blood_bank.latitude, blood_bank.longitude])
      query = "#{blood_bank.latitude},#{blood_bank.longitude}"
      # get location componenets from geocoder
      # first_result = Geocoder.search(query).first
      # address =""
      # if first_result.present?
      #   # get the city name
      #   blood_bank.city_name = first_result.city
      # end
    end
    if @blood_banks.first.present?
      @blood_banks = @blood_banks.sort_by{|data| data.distance}
    else
      return render nothing: true, status: 204
    end
    # return render json: result
  end

end
