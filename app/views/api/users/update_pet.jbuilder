json.id @user.id
json.name @user.name
json.email @user.email
json.phone_no @user.phone_no
json.birth_date @user.birth_date.present? ? @user.birth_date : ''
json.gender @user.gender.present? ? @user.gender : ''
json.latitude @user.latitude
json.longitude @user.longitude
json.profile_photo_url @user.profile_photo.present? ? @user.profile_photo.url : ''
json.profile_photo_thumb_url @user.profile_photo.present? ? @user.profile_photo.thumb.url : ''
json.blood_type_id @user.blood_type_id
json.blood_rh_type_id @user.blood_rh_type_id
json.fb_id @user.fb_id.present? ? @user.fb_id : ''
json.auth_token @user.auth_token
json.address @user.address
json.about @user.about.present? ? @user.about : ''
json.has_pet @user.has_pet.present? ? @user.has_pet : false
json.pet @user.pet.present? ? @user.pet : ''
