json.array! @blood_banks do |bank|
  json.id bank.id
  json.name bank.name
  json.phone_no bank.phone_no.present? ? bank.phone_no : ''
  json.latitude bank.latitude
  json.longitude bank.longitude
  json.address bank.address.present? ? bank.address : ''
  json.distance bank.distance
  json.city_name bank.city_name.present? ? bank.city_name : ''
end
