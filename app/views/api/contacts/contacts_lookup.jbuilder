json.array! @users do |r|
  json.id r.id
  json.name r.name
  json.phone_no r.phone_no.present? ? r.phone_no : ''
  json.profile_photo_url r.profile_photo.present? ? r.profile_photo.url : ''
  json.profile_photo_thumb_url r.profile_photo.present? ? r.profile_photo.thumb.url : ''
  json.blood_type_id r.blood_type_id
  json.blood_rh_type_id r.blood_rh_type_id
  json.address r.distance.present? ? r.address : ''
  json.distance r.distance.present? ? r.distance : ''
  json.city_name r.city_name.present? ? r.city_name : ''
end
