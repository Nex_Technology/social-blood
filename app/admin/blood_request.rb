ActiveAdmin.register BloodRequest do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
permit_params :user_id, :request_sender_id, :accept_flg

  index do
    selectable_column
    id_column
    column :user_id
    column :request_sender_id
    column :accept_flg
    actions
  end

  filter :user_id
  filter :request_sender_id
  filter :accept_flg

  form do |f|
    f.inputs "New Blood Request" do
      f.input :user_id
      f.input :request_sender_id
      f.input :accept_flg
    end
    f.actions
  end

end
