ActiveAdmin.register BloodBank do

permit_params :name, :phone_no, :address, :city_name, :latitude, :longitude

  active_admin_importable do |model, hash|
    hash[:name] = hash[:name].force_encoding('UTF-8')
    unless hash[:phone_no].blank?
      hash[:phone_no] = hash[:phone_no].force_encoding('UTF-8')
    end
    unless hash[:address].blank?
      hash[:address] = hash[:address].force_encoding('UTF-8')
    end
    unless hash[:city_name].blank?
      hash[:city_name] = hash[:city_name].force_encoding('UTF-8')
    end
    unless hash[:latitude].blank?
      hash[:latitude] = hash[:latitude].force_encoding('UTF-8')
    else
      hash[:latitude] = 0.0
    end
    unless hash[:longitude].blank?
      hash[:longitude] = hash[:longitude].force_encoding('UTF-8')
    else
      hash[:longitude] = 0.0
    end
    model.create!(hash)
  end

  index do
    selectable_column
    id_column
    column :id
    column :name
    column :phone_no
    column :address
    column :city_name
    column :latitude
    column :longitude
    actions
  end

  filter :name

  form do |f|
    f.inputs "New Blood Bank" do
      f.input :name
      f.input :phone_no
      f.input :address
      f.input :city_name
      f.input :latitude
      f.input :longitude
    end
    f.actions
  end

end
