class CreateBloodNeeds < ActiveRecord::Migration
  def change
    create_table :blood_needs do |t|
      t.string :location
      t.string :phone_no
      t.string :about
      t.integer :blood_type_id
      t.integer :blood_rh_type_id

      t.timestamps null: false
    end
  end
end
