class AddLatitudeAndLongitudeToBloodNeeds < ActiveRecord::Migration
  def change
    add_column :blood_needs, :latitude, :float
    add_column :blood_needs, :longitude, :float
  end
end
