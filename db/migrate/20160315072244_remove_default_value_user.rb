class RemoveDefaultValueUser < ActiveRecord::Migration
  def self.up
    change_column_default(:users, :name, nil)
    change_column_default(:users, :phone_no, nil)
    change_column_default(:users, :blood_type_id, nil)
    change_column_default(:users, :blood_rh_type_id, nil)
    change_column_default(:users, :latitude, nil)
    change_column_default(:users, :longitude, nil)
    change_column_default(:users, :address, nil)
  end

  def self.down
    change_column :users, :name, :string, :default => ""
    change_column :users, :phone_no, :string, :default => ""
    change_column :users, :blood_type_id, :integer, :default => 0
    change_column :users, :blood_rh_type_id, :integer, :default => 0
    change_column :users, :latitude, :float, :default => 0
    change_column :users, :longitude, :float, :default => 0
    change_column :users, :address, :string, :default => ""
  end
end
