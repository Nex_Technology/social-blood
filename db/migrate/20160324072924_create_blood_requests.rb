class CreateBloodRequests < ActiveRecord::Migration
  def change
    create_table :blood_requests do |t|
      t.string :user_id
      t.string :request_sender_id
      t.boolean :accept_flg

      t.timestamps null: false
    end
  end
end
