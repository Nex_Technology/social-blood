class CreateTownships < ActiveRecord::Migration
  def change
    create_table :townships do |t|
      t.string :name
      t.integer :state_id
      t.float :latitude
      t.float :longitude

      t.timestamps null: false
    end
  end
end
