class ChangeStatetoStateDivisionInTownship < ActiveRecord::Migration
  def change
    rename_column :townships, :state_id, :state_division_id
  end
end
