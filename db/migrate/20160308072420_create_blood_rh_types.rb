class CreateBloodRhTypes < ActiveRecord::Migration
  def change
    create_table :blood_rh_types do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
