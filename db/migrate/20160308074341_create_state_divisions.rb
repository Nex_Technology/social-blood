class CreateStateDivisions < ActiveRecord::Migration
  def change
    create_table :state_divisions do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
