class AddPetColumnsToUser < ActiveRecord::Migration
  def change
    add_column :users, :has_pet, :boolean,  :default => false
    add_column :users, :pet, :string
  end
end
