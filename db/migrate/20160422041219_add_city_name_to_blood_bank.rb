class AddCityNameToBloodBank < ActiveRecord::Migration
  def change
    add_column :blood_banks, :city_name, :string
  end
end
